﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ec_th2012_c.Startup))]
namespace ec_th2012_c
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
