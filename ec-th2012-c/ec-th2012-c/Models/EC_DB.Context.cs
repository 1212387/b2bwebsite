﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ec_th2012_c.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class EC_DBEntities : DbContext
    {
        public EC_DBEntities()
            : base("name=EC_DBEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<BILL2B_BUYSIDE> BILL2B_BUYSIDE { get; set; }
        public virtual DbSet<BILL2B_SELLSIDE> BILL2B_SELLSIDE { get; set; }
        public virtual DbSet<BILL2C> BILL2C { get; set; }
        public virtual DbSet<BILL2C_DETAIL> BILL2C_DETAIL { get; set; }
        public virtual DbSet<CARTS> CARTS { get; set; }
        public virtual DbSet<CATE_DEP> CATE_DEP { get; set; }
        public virtual DbSet<CATELOGIES> CATELOGIES { get; set; }
        public virtual DbSet<COMMENT> COMMENT { get; set; }
        public virtual DbSet<CONTRACT_BUYSIDE> CONTRACT_BUYSIDE { get; set; }
        public virtual DbSet<CONTRACT_SELLSIDE> CONTRACT_SELLSIDE { get; set; }
        public virtual DbSet<DEPARTMENTS> DEPARTMENTS { get; set; }
        public virtual DbSet<INFO> INFO { get; set; }
        public virtual DbSet<LISTPARTNERCONTRACT> LISTPARTNERCONTRACT { get; set; }
        public virtual DbSet<PARTNER_INFO_BUYSIDE> PARTNER_INFO_BUYSIDE { get; set; }
        public virtual DbSet<PRODUCT_INFO> PRODUCT_INFO { get; set; }
        public virtual DbSet<PRODUCTS> PRODUCTS { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<PARTNER_INFO_SELLSIDE> PARTNER_INFO_SELLSIDE { get; set; }
        public virtual DbSet<USER_INFO> USER_INFO { get; set; }
    }
}
