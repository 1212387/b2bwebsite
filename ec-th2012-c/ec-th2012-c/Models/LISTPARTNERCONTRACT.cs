//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ec_th2012_c.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LISTPARTNERCONTRACT
    {
        public int ID { get; set; }
        public Nullable<int> CONTRACTID { get; set; }
        public string PARTNERID { get; set; }
    
        public virtual CONTRACT_BUYSIDE CONTRACT_BUYSIDE { get; set; }
        public virtual PARTNER_INFO_BUYSIDE PARTNER_INFO_BUYSIDE { get; set; }
    }
}
