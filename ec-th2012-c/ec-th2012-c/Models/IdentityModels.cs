﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
namespace ec_th2012_c.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string NAME { get; set; }
        public string ADDRESS { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string ACTIVE { get; set; }
        
       
    }

    public class PARTNER_INFO
    {
        public string ID { get; set; }
       
        public string NAME { get; set; }
        public string DESCRIPTION { get; set; }
        public string CONSUMER_KEY { get; set; }

        public string ACCESSTOKEN { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
        
    }
}