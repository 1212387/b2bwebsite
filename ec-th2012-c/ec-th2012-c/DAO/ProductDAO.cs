﻿using ec_th2012_c.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ec_th2012_c.DAO
{
    public class ProductDAO
    {
        private EC_DBEntities db = new EC_DBEntities();
        public PRODUCTS getProduct(int? id)
        {
            return db.PRODUCTS.Find(id);
        }
    }
}