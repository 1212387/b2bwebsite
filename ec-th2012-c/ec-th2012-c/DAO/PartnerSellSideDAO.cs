﻿using ec_th2012_c.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ec_th2012_c.DAO
{
    public class PartnerSellSideDAO
    {
        private EC_DBEntities db = new EC_DBEntities();
        public List<PARTNER_INFO_SELLSIDE> getListParter()
        {
            return db.PARTNER_INFO_SELLSIDE.ToList();
        }

        internal object getDetailPartner(int? id)
        {
            return db.PARTNER_INFO_SELLSIDE.Find(id);
        }

        internal void editPartner(PARTNER_INFO_SELLSIDE model)
        {
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
        }

        internal void createPartnerSellSide(PARTNER_INFO_SELLSIDE model)
        {
            db.PARTNER_INFO_SELLSIDE.Add(model);
            db.SaveChanges();
        }

        internal void deletePartnerSellSide(int? id)
        {
            PARTNER_INFO_SELLSIDE partner = db.PARTNER_INFO_SELLSIDE.Find(id);
            db.PARTNER_INFO_SELLSIDE.Remove(partner);
            db.SaveChanges();
        }
    }
}