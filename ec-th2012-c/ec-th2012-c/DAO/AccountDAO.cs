﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ec_th2012_c.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Mail;
using System.Net;

namespace ec_th2012_c.DAO
{
    public class AccountDAO
    {
        private EC_DBEntities db = new EC_DBEntities();


        internal bool generateConsumerKey(string guid)
        {
            var currentUser = db.PARTNER_INFO_BUYSIDE.Find(HttpContext.Current.User.Identity.GetUserId());
            if (sendMail(guid, currentUser.AspNetUsers.EMAIL))
            {
                
                currentUser.CONSUMER_KEY = guid;
                db.SaveChanges();
                return true;
            }

            return false;
            
        }

        private bool sendMail(string consumer_key, string email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("ecth2012c@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Ecth2012c new Consumer Key";
                mail.Body = @"<h3>New Consumer Key</h3>
                             
                             <strong>Đây là mã consumer key mới của bạn:</strong>
                             <br>
                             <p>" + consumer_key + @"<p>";

                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;

                SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
                SmtpServer.EnableSsl = true;
                
                SmtpServer.Send(mail);
                return true;
            }
            catch { return false; }
        }

        internal  bool recovertPassword(string username)
        {
            var user = db.AspNetUsers.SingleOrDefault(m => m.UserName == username);
           
            var pass = Guid.NewGuid().ToString("N").Substring(0, 10);

            if (changePassword(user.Id, pass, user.EMAIL))
            {
                
                    return true;
                
            }
            return false;
           
        }

        private bool changePassword(string userID, string pass, string email)
        {
            
            var manager = new UserManager<ApplicationUser>(
                                   new UserStore<ApplicationUser>(
                                   new ApplicationDbContext()));
            manager.RemovePassword(userID);
            IdentityResult result = manager.AddPassword(userID, pass);
            
            if (result.Succeeded)
            {
                sendPassword(pass, email);
                return true;
            }
            return false;
        }

        private bool sendPassword(string pass, string email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("ecth2012c@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Ecth2012c new password";
                mail.Body = @"<h3>New Consumer Key</h3>
                             
                             <strong>Đây là mật khẩu mới của bạn:</strong>
                             <br>
                             <p>" + pass + @"<p>
                                  <br>
                             <strong>Vui lòng truy cập vào trang web và tiến hành thay đổi mật khẩu</strong>";

                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;

                SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch { return false; }
        }
    }
}