﻿using ec_th2012_c.Code;
using ec_th2012_c.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace ec_th2012_c.DAO
{
    public class ContractDAO
    {
        private EC_DBEntities db = new EC_DBEntities();

        public PARTNER_INFO_BUYSIDE getPartnerByConsumerKey(string consumer_key, string iduser)
        {
            //var username = (string)HttpContext.Current.User.Identity.Name;


            //var partner = db.AspNetUsers.SingleOrDefault(m => m.Id == iduser);
            //if (partner.PARTNER_INFO_BUYSIDE.CONSUMER_KEY != consumer_key)
            //    return null;


            var partner = db.PARTNER_INFO_BUYSIDE.SingleOrDefault(m => m.CONSUMER_KEY == consumer_key);
            if (partner == null)
                return null;

            return partner;
        }

        public bool checkShippingParam(objectShippingParam param, string userid)
        {

            var partner = getPartnerByConsumerKey(param.consumer_key, userid);
            if (partner == null)
                return false;

            var contract = partner.AspNetUsers.CONTRACT_BUYSIDE.SingleOrDefault(m => m.ID == Int32.Parse(param.order_id));
            if (contract == null)
                return false;

            if (contract.PRODUCTID != Int32.Parse(param.product_id))
                return false;

            if (contract.MINPROVIDE > param.product_quantity)
                return false;

            return true;
        }

        public void receiveShipping(objectShippingParam param)
        {
            BILL2B_BUYSIDE newBill = new BILL2B_BUYSIDE();
            newBill.DATECREATE = DateTime.Now.ToShortDateString();
            newBill.STATE = "đang giao hàng";
            newBill.CONTRACTID = Int32.Parse(param.order_id);
            newBill.QUANLITY = param.product_quantity;
            newBill.PRODUCT_DATE = param.product_date;
            db.BILL2B_BUYSIDE.Add(newBill);
            db.SaveChanges();
        }

        public List<orderItem> getOrders(string consumer_key)
        {
            var partner = db.PARTNER_INFO_BUYSIDE.SingleOrDefault(m => m.CONSUMER_KEY == consumer_key);

            List<orderItem> listContract = new List<orderItem>();
            foreach (CONTRACT_BUYSIDE item in partner.AspNetUsers.CONTRACT_BUYSIDE.ToList())
            {
                orderItem temp = new orderItem();
                temp.order_id = item.ID.ToString();
                temp.product_id = item.PRODUCTID.ToString();
                temp.product_name = item.PRODUCTS.NAME;
                temp.product_quantity = (int)item.PRODUCTS.QUANTITY;
                listContract.Add(temp);

            }
            return listContract;
        }


        public List<CONTRACT_SELLSIDE> getListContractSellSide()
        {
            checkStateProductQuantityPartner();
            return db.CONTRACT_SELLSIDE.ToList();
        }

        public CONTRACT_SELLSIDE getContractSellSideByID(int? id)
        {
            return db.CONTRACT_SELLSIDE.Find(id);
        }

        private CONTRACT_SELLSIDE getContractSellSide(string orderid, int partnerid)
        {
            return db.CONTRACT_SELLSIDE.SingleOrDefault(m => m.ORDER_ID_PARTNER == orderid && m.PARTNERID == partnerid);

        }

        public void checkStateProductQuantityPartner()
        {
            var listPartner = db.PARTNER_INFO_SELLSIDE.ToList();
            foreach (var item in listPartner)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(item.WEBSITE);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", item.USERNAME, item.PASSWORD))));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // New code:
                    HttpResponseMessage response = client.GetAsync(item.LINK_ORDERS + item.CONSUMER_KEY).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var orders = response.Content.ReadAsAsync<List<orderItem>>().Result;
                        foreach (orderItem itemcontract in orders)
                        {
                            var contract = getContractSellSide(itemcontract.order_id, item.ID);
                            if (contract != null)
                            {
                                contract.PRODUC_ID_PARTNER = itemcontract.product_id;
                                contract.PRODUCT_NAME_PARTNER = itemcontract.product_name;
                                contract.PRODUCT_QUANTITY_PARTNER = itemcontract.product_quantity;
                                contract.TIMEUPDATE = DateTime.Now.ToString();
                                if (contract.MININVENTORY > contract.PRODUCT_QUANTITY_PARTNER)
                                {
                                    contract.STATE = "hết hàng";
                                }
                                else
                                {
                                    contract.STATE = "còn hàng";
                                }
                                db.SaveChanges();
                            }
                        }


                    }

                }
            }
        }

        internal bool startShipping(int id, int quantity, string date)
        {
            var contract = getContractSellSideByID(id);

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(contract.PARTNER_INFO_SELLSIDE.WEBSITE);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", contract.PARTNER_INFO_SELLSIDE.USERNAME, contract.PARTNER_INFO_SELLSIDE.PASSWORD))));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                var temp = new objectShippingParam() { consumer_key = contract.PARTNER_INFO_SELLSIDE.CONSUMER_KEY, order_id = contract.ORDER_ID_PARTNER, product_quantity = quantity, product_date = date, product_id = contract.PRODUC_ID_PARTNER };

                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync(contract.PARTNER_INFO_SELLSIDE.LINK_SHIPPING, temp).Result;
                if (response.IsSuccessStatusCode)
                {
                    saveBillSellSide(id, quantity, date);
                    return true;
                }
                else
                {
                    return false;
                }
            }


        }

        private void saveBillSellSide(int id, int quantity, string date)
        {
            var contract = getContractSellSideByID(id);
            contract.PRODUCTS.QUANTITY = contract.PRODUCTS.QUANTITY - quantity;
            db.SaveChanges();

            BILL2B_SELLSIDE bill = new BILL2B_SELLSIDE();
            bill.CONTRACTID = id;
            bill.QUANLITY = quantity;
            bill.STATE = "đang giao hàng";
            bill.DATECREATE = DateTime.Today.ToShortDateString();
            bill.DATERECEIVED = date;
            db.BILL2B_SELLSIDE.Add(bill);
            db.SaveChanges();

            sendMailShipping(contract, bill.ID, bill.QUANLITY, bill.DATERECEIVED, bill.CONTRACT_SELLSIDE.PARTNER_INFO_SELLSIDE.EMAIL);
        }

        private void sendMailShipping(CONTRACT_SELLSIDE contract, int id, int quantity, string date, string email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("ecth2012c@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Ecth2012c confirm shipping";
                mail.Body = @"<h3>Chào đối tác</h3>
                             
                             <strong>Đây là thông tin giao hàng</strong>
                             <br>
                            <p>Tên sản phẩm: </p>
                             <p>" + contract.PRODUCT_NAME_PARTNER + @"</p>
                             <br>
                                <p>Mã hợp đồng: </p>
                             <p>" + contract.ORDER_ID_PARTNER + @"</p>
                             <br>
                             <p>Số lượng giao: </p>
                             <p>" + quantity + @"</p>
                             <br>
                               <p>Ngày nhận: </p>
                             <p>" + date + @"</p>
                                <br>
                               <p>Vui lòng nhấp vào link sau để xác nhận đã nhận hàng: </p>
                             <a href='http://ecth2012c.somee.com/AdminHome/shippingOk/" + id + @"'>Đã nhận hàng </a>";

                mail.IsBodyHtml = true;
                SmtpServer.Port = 587;

                SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c@gmail.com", "tmdt123456");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
            }
            catch
            {

            }
        }

        internal List<BILL2B_SELLSIDE> getlistBillSellSide(int? id)
        {
            var contract = getContractSellSideByID(id);
            return contract.BILL2B_SELLSIDE.ToList();
        }

        internal List<BILL2B_SELLSIDE> getlistBillSellSide()
        {

            return db.BILL2B_SELLSIDE.ToList();
        }

        internal void shippingOk(int? id)
        {
            var bill = db.BILL2B_SELLSIDE.Find(id);
            bill.STATE = "đã giao hàng";
            db.SaveChanges();
        }

        public SelectList selectListPartner(int? id)
        {
            SelectList list = new SelectList(db.PARTNER_INFO_SELLSIDE, "ID", "NAME_PARTNER", id);
            return list;
        }

        public SelectList selectListProduct(int? id)
        {
            SelectList list = new SelectList(db.PRODUCTS, "ID", "NAME", id);
            return list;
        }

        internal void editContract(CONTRACT_SELLSIDE cONTRACT_SELLSIDE)
        {
            db.Entry(cONTRACT_SELLSIDE).State = EntityState.Modified;
            db.SaveChanges();
        }

        internal void deleteContractSellSide(int? id)
        {
            CONTRACT_SELLSIDE cONTRACT_SELLSIDE = db.CONTRACT_SELLSIDE.Find(id);
            foreach (var item in cONTRACT_SELLSIDE.BILL2B_SELLSIDE.ToList())
            {
                db.BILL2B_SELLSIDE.Remove(item);
            }
            db.CONTRACT_SELLSIDE.Remove(cONTRACT_SELLSIDE);
            db.SaveChanges();

        }

        internal void createContractSellSide(CONTRACT_SELLSIDE cONTRACT_SELLSIDE)
        {
            db.CONTRACT_SELLSIDE.Add(cONTRACT_SELLSIDE);
            db.SaveChanges();
        }

        internal List<CONTRACT_SELLSIDE> getContractByPartner(int? id)
        {

            var partner = db.PARTNER_INFO_SELLSIDE.Find(id);
            return partner.CONTRACT_SELLSIDE.ToList();
        }

        internal bool shippingOkFromMail(int? id)
        {
            var currentUser = HttpContext.Current.User.Identity.GetUserId();
            var bill = db.BILL2B_SELLSIDE.Find(id);
            if (bill == null)
            {
                return false;
            }
            bill.STATE = "đã giao hàng";
            db.SaveChanges();
            return true;


        }

        public bool receiveShipping(int? id)
        {
            var bill = db.BILL2B_BUYSIDE.Find(id);
            if (bill == null)
                return false;
            bill.CONTRACT_BUYSIDE.PRODUCTS.QUANTITY += bill.QUANLITY;
            bill.STATE = "đã nhận hàng";
            db.SaveChanges();
            return true;
        }

    }
}