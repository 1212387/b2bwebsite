﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ec_th2012_c.Models;
using System.Data.Entity;
namespace ec_th2012_c.DAO
{
    public class PartnerBuySideDAO
    {
        private EC_DBEntities db = new EC_DBEntities();


        internal List<PARTNER_INFO_BUYSIDE> getListPartner()
        {
            return db.PARTNER_INFO_BUYSIDE.ToList();
        }

        internal void changeActivePartner(string id)
        {
            var partner = findPartner(id);
            if (partner == null)
                return;
            if (partner.AspNetUsers.ACTIVE == "Active")
            {
                partner.AspNetUsers.ACTIVE = "Deactivate";
            }
            else
            {
                partner.AspNetUsers.ACTIVE = "Active";

            }
            db.SaveChanges();
        }

        public PARTNER_INFO_BUYSIDE findPartner(string id)
        {
            return db.PARTNER_INFO_BUYSIDE.Find(id);
        }

        internal void editPartner(PARTNER_INFO_BUYSIDE model)
        {
            var partner = findPartner(model.ID);
            partner.CONSUMER_KEY = model.CONSUMER_KEY;
            partner.DESCRIPTION = model.DESCRIPTION;
            partner.NAME = model.NAME;
            partner.AspNetUsers.ACTIVE = model.AspNetUsers.ACTIVE;
            partner.AspNetUsers.ADDRESS = model.AspNetUsers.ADDRESS;
            partner.AspNetUsers.EMAIL = model.AspNetUsers.EMAIL;
            partner.AspNetUsers.NAME = model.AspNetUsers.NAME;
            partner.AspNetUsers.PHONE = model.AspNetUsers.PHONE;
            db.SaveChanges();
        }

        internal void deletePartner(string id)
        {
            var partner = findPartner(id);
            foreach (var item in partner.LISTPARTNERCONTRACT.ToList())
            {
                db.LISTPARTNERCONTRACT.Remove(item);
            }
            foreach (var item in partner.AspNetUsers.CONTRACT_BUYSIDE.ToList())
            {
                foreach (var itemBill in item.BILL2B_BUYSIDE.ToList())
                {
                    db.BILL2B_BUYSIDE.Remove(itemBill);
                }
                db.CONTRACT_BUYSIDE.Remove(item);
            }
            db.PARTNER_INFO_BUYSIDE.Remove(partner);
            
            var aspnetuser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspnetuser);
            db.SaveChanges();
        }
    }
}