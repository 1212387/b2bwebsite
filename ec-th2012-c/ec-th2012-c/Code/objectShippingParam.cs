﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ec_th2012_c.Code
{
    public class objectShippingParam
    {
        public string consumer_key { get; set; }
        public string order_id { get; set; }
        public string product_id { get; set; }
        public int product_quantity { get; set; }
        public string product_date { get; set; }
    }
}