﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Net.Mail;
using ec_th2012_c.DAO;

namespace ec_th2012_c.Controllers
{

    public class B2BBuySideController : Controller
    {
        private EC_DBEntities db = new EC_DBEntities();
        private ContractDAO contractDAO = new ContractDAO();

        // GET: /B2BBuySide/
        //TRANG HIỂN THỊ CÁC DANH SÁCH HỢP ĐỒNG CHƯA CÓ ĐỐI TÁC VÀ ĐỐI TÁC CHƯA ĐĂNG KÝ

        [Authorize(Roles = "Admin")]
        public ActionResult getAllListBill()
        {
            return View("listBill", db.BILL2B_BUYSIDE.ToList());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult shippingOk(int? id)
        {
            contractDAO.receiveShipping(id);
            return View("listBill", db.BILL2B_BUYSIDE.ToList());
        }

        public ActionResult Index()
        {
            List<CONTRACT_BUYSIDE> contract_buyside;
            contract_buyside = db.CONTRACT_BUYSIDE.Include(c => c.AspNetUsers).Include(c => c.PRODUCTS).Where(c=>!c.STATE.Equals("đang hợp tác")).ToList();
            if (User.IsInRole("Partner"))
            {
                var user = GetCurrentUser();
                List<LISTPARTNERCONTRACT> partner_contract = db.LISTPARTNERCONTRACT.Where(c => c.PARTNER_INFO_BUYSIDE.ID.Equals(user.Id)).ToList();
                foreach (LISTPARTNERCONTRACT item in partner_contract)
                {
                    contract_buyside.RemoveAll(p => p.ID == item.CONTRACTID);
                }
            }
            return View(contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        private ApplicationUser GetCurrentUser()
        {
            var manager = new UserManager<ApplicationUser>(
                                    new UserStore<ApplicationUser>(
                                    new ApplicationDbContext()));
            // Find user
            var user = manager.FindById(User.Identity.GetUserId());
            return user;
        }

        [AllowAnonymous]
        // GET: /B2BBuySide/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_BUYSIDE contract_buyside = db.CONTRACT_BUYSIDE.Find(id);
            if (contract_buyside == null)
            {
                return HttpNotFound();
            }
            return View(contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        // GET: /B2BBuySide/Create
        public ActionResult Create()
        {
            ViewBag.PRODUCTID = new SelectList(db.PRODUCTS, "ID", "NAME");
            return View();
        }

        // POST: /B2BBuySide/CreateContract
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateContract([Bind(Include="PRODUCTID,MININVENTORY,MINPROVIDE,DELAYTIME,COOPTIME,BILLTIME,PRICE,STARTDAY,ENDDAY")] CONTRACT_BUYSIDE contract_buyside)
        {
            if (ModelState.IsValid)
            {
                db.CONTRACT_BUYSIDE.Add(contract_buyside);
                db.SaveChanges();
                return RedirectToAction("ContractManager");
            }

            ViewBag.PRODUCTID = new SelectList(db.PRODUCTS, "ID", "NAME", contract_buyside.PRODUCTID);
            return View("Create",contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        // GET: /B2BBuySide/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_BUYSIDE contract_buyside = db.CONTRACT_BUYSIDE.Find(id);
            if (contract_buyside == null)
            {
                return HttpNotFound();
            }
            ViewBag.PRODUCTID = new SelectList(db.PRODUCTS, "ID", "NAME", contract_buyside.PRODUCTID);
            return View(contract_buyside);
        }

        // POST: /B2BBuySide/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,PRODUCTID,MININVENTORY,MINPROVIDE,DELAYTIME,COOPTIME,BILLTIME,PRICE,STARTDAY,ENDDAY")] CONTRACT_BUYSIDE contract_buyside)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contract_buyside).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ContractManager");
            }
            
            ViewBag.PRODUCTID = new SelectList(db.PRODUCTS, "ID", "NAME", contract_buyside.PRODUCTID);
            return View(contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        // GET: /B2BBuySide/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_BUYSIDE contract_buyside = db.CONTRACT_BUYSIDE.Find(id);
            if (contract_buyside == null)
            {
                return HttpNotFound();
            }
            return View(contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        // POST: /B2BBuySide/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CONTRACT_BUYSIDE contract_buyside = db.CONTRACT_BUYSIDE.Find(id);
            List<BILL2B_BUYSIDE> listbill = db.BILL2B_BUYSIDE.Where(m => m.CONTRACTID == id).ToList();
            db.BILL2B_BUYSIDE.RemoveRange(listbill);
            db.CONTRACT_BUYSIDE.Remove(contract_buyside);
            List<LISTPARTNERCONTRACT> tmp = db.LISTPARTNERCONTRACT.Where(c => c.CONTRACTID == id).ToList();
            db.LISTPARTNERCONTRACT.RemoveRange(tmp);
            db.SaveChanges();
            return RedirectToAction("ContractManager");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize(Roles = "Partner")]
        public ActionResult ChosenByPartner(int id)
        {
            var user = GetCurrentUser();
            List<LISTPARTNERCONTRACT> tmp = db.LISTPARTNERCONTRACT.Where(c => c.CONTRACTID == id && c.PARTNER_INFO_BUYSIDE.ID.Equals(user.Id)).ToList();
            if (tmp.Count == 0)
            {
                LISTPARTNERCONTRACT partner_contract = new LISTPARTNERCONTRACT();
                partner_contract.CONTRACTID = id;
                //***Đáng lẽ ở đây chỉ lưu PARTNERID nhưng do chưa xong phần Đăng nhập nên set cứng
                partner_contract.PARTNER_INFO_BUYSIDE = db.PARTNER_INFO_BUYSIDE.Where(c => c.ID.Equals(user.Id)).FirstOrDefault();
                partner_contract.PARTNERID = user.Id;
                //
                db.LISTPARTNERCONTRACT.Add(partner_contract);
                db.SaveChanges();
                ViewData["isChoosingSuccess"] = "Đăng ký hợp đồng thành công! Hợp đồng sẽ được đưa vào danh sách chờ xét duyệt!";
                List<CONTRACT_BUYSIDE> contract_buyside = db.CONTRACT_BUYSIDE.Include(c => c.AspNetUsers).Include(c => c.PRODUCTS).Where(c=>!c.STATE.Equals("đang hợp tác")).ToList();
                List<LISTPARTNERCONTRACT> partner_contract_tmp = db.LISTPARTNERCONTRACT.Where(c => c.PARTNER_INFO_BUYSIDE.ID.Equals(user.Id)).ToList();
                foreach (LISTPARTNERCONTRACT item in partner_contract_tmp)
                {
                    contract_buyside.RemoveAll(p => p.ID == item.CONTRACTID);
                }
                return View("Index", contract_buyside);
            }
            return RedirectToAction("Index","B2BBuySide");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ContractManager()
        {
            List<CONTRACT_BUYSIDE> contract_buyside = db.CONTRACT_BUYSIDE.Include(t => t.PRODUCTS).Include(c=>c.LISTPARTNERCONTRACT).ToList();
            List<String> listResult = new List<string>();
            foreach(var item in contract_buyside)
            {
                if (item.STATE != null && item.STATE.Equals("đang hợp tác"))
                {
                    listResult.Add("Đang hợp tác với " 
                        + db.PARTNER_INFO_BUYSIDE.Find(item.PARTNERID).NAME);
                }
                else
                {
                    listResult.Add("Đã có " + item.LISTPARTNERCONTRACT.Where(c => c.CONTRACTID == item.ID).ToList().Count + " đối tác");
                }
            }
            ViewData["listResult"] = listResult;
            return View(contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DetailForAdmin(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_BUYSIDE contract_buyside = db.CONTRACT_BUYSIDE.Find(id);
            if (contract_buyside == null)
            {
                return HttpNotFound();
            }
            var contract_partner = db.LISTPARTNERCONTRACT.Where(c => c.CONTRACTID == id);
            ViewData["contract_partner"] = contract_partner.ToList();
            return View("DetailForAdmin",contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult DetailForSignedContract(int id)
        {
            CONTRACT_BUYSIDE contract_buyside = db.CONTRACT_BUYSIDE.Find(id);
            if (contract_buyside == null)
            {
                return HttpNotFound();
            }
            var contract_partner = db.LISTPARTNERCONTRACT.Where(c => c.CONTRACTID == id);
            ViewData["contract_partner"] = contract_partner.ToList();
            return View("DetailForSignedContract", contract_buyside);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult SignContract(int id)
        {
            LISTPARTNERCONTRACT contract_partner = db.LISTPARTNERCONTRACT.Find(id);
            CONTRACT_BUYSIDE contract = db.CONTRACT_BUYSIDE.Find(contract_partner.CONTRACTID);
            contract.PARTNERID = contract_partner.PARTNERID;
            contract.STATE = "đang hợp tác";
            contract.AspNetUsers = db.AspNetUsers.Find(contract_partner.PARTNERID);
            db.Entry(contract).State = EntityState.Modified;
            db.SaveChanges();
            
            try
            {
                SendMailToPartner(contract_partner.PARTNER_INFO_BUYSIDE.AspNetUsers.EMAIL, contract);
            }
            catch
            {

            }
            return RedirectToAction("ContractManager");
        }


        [Authorize(Roles = "Admin")]
        private void SendMailToPartner(String partnerEmail,CONTRACT_BUYSIDE contract)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("ecth2012c@gmail.com");
            mail.To.Add(partnerEmail);
            mail.Subject = "Ký kết hợp đồng cho sản phẩm " + contract.PRODUCTS.NAME;
            mail.IsBodyHtml = true;
            mail.Body = @"<strong>Kiểm tra số lượng tồn của các sản phẩm thuộc các hợp đồng hiện tại :</strong>
                             <p>ecth2012c.somee.com/orders</p>
                             <br>
                             <strong>Xác nhận giao hàng :</strong>
                             <p>ecth2012c.somee.com/orders/startshipping</p>
                             <br>
                             <strong>THÔNG TIN SẢN PHẨM</strong>
                             <br>
                             <strong>Tên sản phẩm: </strong>
                             <p>" + contract.PRODUCTS.NAME + @"<p>
                             <br>
                             <strong>Mã sản phẩm: </strong>
                             <p>" + contract.PRODUCTID.ToString() + @"<p>
                             <br>
                             <strong>Ngưỡng tồn kho tối thiểu: </strong>
                             <p>" + contract.MININVENTORY.ToString() + @"<p>
                             <br>
                             <strong>Số lượng cần cung cấp khi kho đạt ngưỡng tối thiểu: </strong>
                             <p>" + contract.MINPROVIDE.ToString() + @"<p>
                             <br>
                             <strong>Thời gian giao hàng khi xảy ra ngưỡng tối thiểu: </strong>
                             <p>" + contract.DELAYTIME + @"<p>
                             <br>
                             <strong>Thời gian hợp tác: </strong>
                             <p>" + contract.COOPTIME + @"<p>
                             <br>
                             <strong>Thời gian chuyển tiền: </strong>
                             <p>" + contract.BILLTIME + @"</p> 
                             <br>
                             <strong>Consumer Key: </strong>
                             <p>" + contract.AspNetUsers.PARTNER_INFO_BUYSIDE.CONSUMER_KEY + "</p>";



            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("ecth2012c", "tmdt123456");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }

        [Authorize(Roles = "Partner")]
        public ActionResult ContractResultForPartner()
        {
            List<String> listResult = new List<String>();
            List<CONTRACT_BUYSIDE> contract_buyside;
            List<CONTRACT_BUYSIDE> listContractPartner = new List<CONTRACT_BUYSIDE>();
            contract_buyside = db.CONTRACT_BUYSIDE.Include(c => c.AspNetUsers).Include(c => c.PRODUCTS).ToList();
            if (User.IsInRole("Partner"))
            {
                var user = GetCurrentUser();
                List<LISTPARTNERCONTRACT> partner_contract = db.LISTPARTNERCONTRACT.Where(c => c.PARTNER_INFO_BUYSIDE.ID.Equals(user.Id)).ToList();
                foreach (LISTPARTNERCONTRACT item in partner_contract)
                {
                    CONTRACT_BUYSIDE tmp = contract_buyside.
                                     Where(p => p.ID == item.CONTRACTID).FirstOrDefault();
                    listContractPartner.Add(tmp);
                    if(tmp.STATE != null && tmp.STATE.Equals("đang hợp tác"))
                    {
                        if(tmp.PARTNERID == user.Id)
                        {
                            listResult.Add("Đang hợp tác");
                        }
                        else
                        {
                            listResult.Add("Ký kết thất bại");
                        }
                    }
                    else
                    {
                        listResult.Add("Đang chờ duyệt");
                    }
                }
                ViewData["listResults"] = listResult;
            }
            return View(listContractPartner);
        }

        [Authorize(Roles = "Partner")]
        public ActionResult DeleteContractByPartner(int id)
        {
            var user = GetCurrentUser();
            LISTPARTNERCONTRACT tmp = db.LISTPARTNERCONTRACT.Where(c=>c.CONTRACTID == id 
                                                                    && c.PARTNERID.Equals(user.Id)).FirstOrDefault();
            db.LISTPARTNERCONTRACT.Remove(tmp);
            db.SaveChanges();
            return RedirectToAction("ContractResultForPartner", "B2BBuySide");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult listBillBuySide(int? id)
        {
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = db.CONTRACT_BUYSIDE.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model.BILL2B_BUYSIDE.ToList());
        }

        [Authorize(Roles = "Admin")]
        public ActionResult shippingOkInContract(int? idContract, int? idBill)
        {
            contractDAO.receiveShipping(idBill);
            return RedirectToAction("listBillBuySide", new { id = idContract });
        }
    }
}
