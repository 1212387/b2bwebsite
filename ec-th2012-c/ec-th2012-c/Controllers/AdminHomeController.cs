﻿using ec_th2012_c.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ec_th2012_c.Controllers
{

    
    public class AdminHomeController : Controller
    {
        private ContractDAO db = new ContractDAO();
        // GET: AdminHome
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            return View("Index");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult returnURL(string returnUrl)
        {
            return Redirect(returnUrl);
        }

       
        public ActionResult shippingOk(int? id)
        {
            if (db.shippingOkFromMail(id))
            {
                ViewData["result"] = "Ghi nhận thành công. Cám ơn";
                
            }
            else
            {
                ViewData["result"] = "Đã có lỗi xảy ra.... vui lòng kiểm tra lại.";
            }
            return View("ShippingOk");
        }

    }
}