﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using ec_th2012_c.DAO;
using ec_th2012_c.Code;
using ec_th2012_c.Filters;

namespace ec_th2012_c.Controllers
{
    [IdentityBasicAuthentication] // Enable Basic authentication for this controller.
    [Authorize] // Require authenticated requests.
    public class ordersAPIController : ApiController
    {

        private ContractDAO contractDAO = new ContractDAO();


        [HttpGet]
        [Route("orders/{consumer_key}")]
        public IHttpActionResult getOrders([FromUri] string consumer_key)
        {
            var userid = User.Identity.GetUserId();
            if (contractDAO.getPartnerByConsumerKey(consumer_key, userid) == null)
                return BadRequest();
            return Ok(contractDAO.getOrders(consumer_key));
        }

        [HttpPost]
        [Route("orders/start_shipping")]
        public IHttpActionResult confirmShipping([FromBody] objectShippingParam parameter)
        {
            var userid = User.Identity.GetUserId();
            if (!contractDAO.checkShippingParam(parameter, userid))
                return BadRequest();
            contractDAO.receiveShipping(parameter);
            return Ok();
        }
    }
}
