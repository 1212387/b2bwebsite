﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.Models;
using ec_th2012_c.DAO;

namespace ec_th2012_c.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        private ProductDAO db = new ProductDAO();
        // GET: Product
        public ActionResult DetailProduct(int? id)
        {
            var model = db.getProduct(id);
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View("Product", model);
        }
    }
}