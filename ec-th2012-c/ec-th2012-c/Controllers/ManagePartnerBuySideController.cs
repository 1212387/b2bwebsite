﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.DAO;
using System.Net;
using ec_th2012_c.Models;


namespace ec_th2012_c.Controllers
{
    [Authorize(Roles="Admin")]
    public class ManagePartnerBuySideController : Controller
    {
        private PartnerBuySideDAO db = new PartnerBuySideDAO();
        private EC_DBEntities dbEntities = new EC_DBEntities();
        // GET: ManagePartnerBuySide
        public ActionResult Index()
        {
            var listPartner = db.getListPartner();
            return View(listPartner);
        }

      
        public ActionResult Active(string id)
        {
            db.changeActivePartner(id);
            return RedirectToAction("Index");
        }



        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var partner = db.findPartner(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View(partner);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var partner = db.findPartner(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View(partner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PARTNER_INFO_BUYSIDE model)
        {
            if (ModelState.IsValid)
            {
                db.editPartner(model);

                return RedirectToAction("Details/"+model.ID);
            }

            return View(model);

        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var partner = db.findPartner(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            db.deletePartner(id);
            return RedirectToAction("Index");
        }

        public ActionResult Contracts(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var partner = db.findPartner(id);
            if (partner == null)
            {
                return HttpNotFound();
            }
            var listContracts = partner.AspNetUsers.CONTRACT_BUYSIDE.ToList();
            List<String> listResult = new List<String>();
            foreach (var item in listContracts)
            {
                listResult.Add("Đang hợp tác");
            }
            ViewData["listResults"] = listResult;
            return View("Contracts", listContracts);
        }

        public ActionResult DetailContract(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_BUYSIDE contract_buyside = dbEntities.CONTRACT_BUYSIDE.Find(id);
            if (contract_buyside == null)
            {
                return HttpNotFound();
            }
            return View(contract_buyside);
        }
    }
}