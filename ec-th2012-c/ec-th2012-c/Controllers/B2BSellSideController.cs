﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.Models;
using ec_th2012_c.DAO;

namespace ec_th2012_c.Controllers
{
    [Authorize(Roles = "Admin")]
    public class B2BSellSideController : Controller
    {
        private ContractDAO db = new ContractDAO();

        //private EC_DBEntities db = new EC_DBEntities();

        // GET: B2BSellSide
        public ActionResult Index()
        {
            var listContract = db.getListContractSellSide();

            return View("Index", listContract);
        }

        //GET: B2BSellSide/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_SELLSIDE cONTRACT_SELLSIDE = db.getContractSellSideByID(id);
            if (cONTRACT_SELLSIDE == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View(cONTRACT_SELLSIDE);
        }

        [HttpGet]
        public ActionResult startShipping(int id, int quantity, string date)
        {
            if (db.startShipping(id, quantity, date))
            {

                return Json("Chuyển hàng thành công", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Chuyển hàng KHÔNG thành công. Vui lòng thử lại", JsonRequestBehavior.AllowGet);
            }


        }

        [HttpGet]
        public ActionResult listBillSellSide(int? id)
        {
            var listBill = db.getlistBillSellSide(id);
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View("listBill", listBill);
        }

        [HttpGet]
        public ActionResult shippingOk(int? idContract, int? idBill)
        {
            db.shippingOk(idBill);
            return RedirectToAction("listBillSellSide", new { id = idContract });
        }

        [HttpGet]
        public ActionResult shippingOkAllList(int? idBill)
        {
            db.shippingOk(idBill);
            return RedirectToAction("getAllListBill");
        }

        // GET: B2BSellSide/Create
        public ActionResult Create()
        {
            ViewBag.PARTNERID = db.selectListPartner(null);
            ViewBag.PRODUCTID = db.selectListProduct(null);
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View();
        }

        // POST: B2BSellSide/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CONTRACT_SELLSIDE cONTRACT_SELLSIDE)
        {

            db.createContractSellSide(cONTRACT_SELLSIDE);

            return RedirectToAction("Index");

        }

        // GET: B2BSellSide/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = db.getContractSellSideByID(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.PARTNERID = db.selectListPartner(model.PARTNERID);
            ViewBag.PRODUCTID = db.selectListProduct(model.PRODUCTID);
            return View(model);
        }

        // POST: B2BSellSide/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CONTRACT_SELLSIDE cONTRACT_SELLSIDE)
        {
            if (ModelState.IsValid)
            {
                db.editContract(cONTRACT_SELLSIDE);

                return RedirectToAction("Details/" + cONTRACT_SELLSIDE.ID);
            }
            ViewBag.PARTNERID = db.selectListPartner(cONTRACT_SELLSIDE.PARTNERID);
            ViewBag.PRODUCTID = db.selectListProduct(cONTRACT_SELLSIDE.PRODUCTID);
            return View(cONTRACT_SELLSIDE);
        }

        // GET: B2BSellSide/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CONTRACT_SELLSIDE cONTRACT_SELLSIDE = db.getContractSellSideByID(id);
            if (cONTRACT_SELLSIDE == null)
            {
                return HttpNotFound();
            }
            db.deleteContractSellSide(id);
            return RedirectToAction("Index");
        }

        public ActionResult getAllListBill()
        {
            var listBill = db.getlistBillSellSide();
           
            return View(listBill);
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
