﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ec_th2012_c.DAO;
using System.Net;
using ec_th2012_c.Models;

namespace ec_th2012_c.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManagePartnerSellSideController : Controller
    {
        private PartnerSellSideDAO dbPartner = new PartnerSellSideDAO();
        private ContractDAO dbContract = new ContractDAO();

        [HttpGet]
        // GET: ManagePartner
        public ActionResult Index()
        {
            var model = dbPartner.getListParter();
            return View("Index", model);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = dbPartner.getDetailPartner(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View("Details", model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = dbPartner.getDetailPartner(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PARTNER_INFO_SELLSIDE model)
        {
            if (ModelState.IsValid)
            {
                dbPartner.editPartner(model);

                return RedirectToAction("Details/" + model.ID);
            }

            return View(model);
        }

        public ActionResult Delete(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cONTRACT_SELLSIDE = dbPartner.getDetailPartner(id);
            if (cONTRACT_SELLSIDE == null)
            {
                return HttpNotFound();
            }
            dbPartner.deletePartnerSellSide(id);
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {

            ViewBag.ReturnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PARTNER_INFO_SELLSIDE model)
        {
            
                dbPartner.createPartnerSellSide(model);

                return RedirectToAction("Index");
            
          

        }

        [HttpGet]
        public ActionResult Contracts(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var cONTRACT_SELLSIDE = dbPartner.getDetailPartner(id);
            if (cONTRACT_SELLSIDE == null)
            {
                return HttpNotFound();
            }
            var model = dbContract.getContractByPartner(id);
            
            return View("../B2BSellSide/Index", model);
        }
    }
}